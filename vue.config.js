module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  pluginOptions: {
    electronBuilder: {
        externals: ['typeorm', 'sequelize', 'sequelize-typescript', 'sqlite3'],
    }
},
}