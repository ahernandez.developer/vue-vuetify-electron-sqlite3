# vue-electron


## Solve please install sqlite3 package manually vue electron vuetify sqlite3
```
Add this to the vue.config.js
 pluginOptions: {
    electronBuilder: {
        externals: ['typeorm', 'sequelize', 'sequelize-typescript', 'sqlite3'],
    }
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
